;
(function() {
    // CommonJS
    SyntaxHighlighter = SyntaxHighlighter || (typeof require !== 'undefined' ? require('shCore').SyntaxHighlighter : null);

    function Brush() {

        // JCL (Job Control Language)

        var keywords = 'JOB EXEC DD SET';

        this.regexList = [ {
            regex : /^\/\/\*(.*)/gm,
            css : 'comments'
        }, // JCL comment
        {
            regex : SyntaxHighlighter.regexLib.singleQuotedString,
            css : 'string'
        }, // strings
        {
            regex : /(\=|\(|\)|,)/g,
            css : 'color2'
        }, // 3270 operator highlighting
        {
            regex : new RegExp(this.getKeywords(keywords), 'gm'),
            css : 'color3'
        } // JCL keywords
        ];

        this.forHtmlScript({
            left : /(&lt;|<)%[@!=]?/g,
            right : /%(&gt;|>)/g
        });
    }
    ;

    Brush.prototype = new SyntaxHighlighter.Highlighter();
    Brush.aliases = [ 'jcl' ];

    SyntaxHighlighter.brushes.JCL = Brush;

    // CommonJS
    typeof (exports) != 'undefined' ? exports.Brush = Brush : null;
})();
